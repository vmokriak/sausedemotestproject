﻿using System;
using Atata;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Globalization;
using SauseDemoUITests.Components;
using Bogus;
using NUnit.Allure.Core;

namespace SauseDemoUITests
{
    [AllureNUnit]
    public class SauseMainTest : UITestFixture
    {
        [Test]
        [TestCase("Sauce Labs Onesie", "7.99", "Rib snap infant onesie for the junior automation engineer in development. " +
            "Reinforced 3-snap bottom closure, two-needle hemmed sleeved " +
            "and bottom won't unravel.", "Tax: $0.64", "Total: $8.63")]
        [TestCase("Sauce Labs Bolt T-Shirt", "15.99", "Get your testing superhero on with the Sauce Labs bolt " +
      "T-shirt. From American Apparel, 100% ringspun combed cotton, " +
      "heather gray with red bolt.", "Tax: $1.28", "Total: $17.27")]
        [TestCase("Sauce Labs Backpack", "29.99", "carry.allTheThings() with the sleek, streamlined Sly " +
      "Pack that melds uncompromising style with unequaled laptop and tablet protection.", "Tax: $2.40", "Total: $32.39")]

        public void Sauce_Demo_Test(

      string productName, string productPrice, string productDesc, string productTax, string productTotal)

        {
            Faker faker = new Faker();
            string firstName = faker.Name.FirstName();
            string lastName = faker.Name.LastName();
            string postalCode = faker.Address.ZipCode();

            var a = Login().
                 ProductsLabel.Should.Equal("Products").
                 Products.Count.Should.Equal(6);

            a.Products.First(x => x.Price.ToString().Contains(productPrice))
           .AddToCartButton.Click().
            ShoppingCartCounter.Should.Equal(1).
            ShoppingCartLink.Click();

            Go.To<YourCartPage>().
                SubHeaderLabelLabel.Should.Equal("Your Cart").
                ShoppingCartCounter.Should.Equal(1).
                ProductNameAddedToCart.Should.Equal(productName).
                ProductDescriptionAddedToCart.Should.Equal(productDesc).
                Price.Should.Contain(productPrice).
                CheckoutButton.Click();

            Go.To<YourCheckoutPage>().
                SubHeaderLabelLabel.Should.Equal("Checkout: Your Information").
                FirstName.Set(firstName).
                LastName.Set(lastName).
                PostalCode.Set(postalCode).
                CartCheckoutButton.Click();

            Go.To<OverviewCheckoutPage>().
                SubHeaderLabelLabel.Should.Equal("Checkout: Overview").
                ShoppingCartCounter.Should.Equal(1).
                ProductNameAddedToCart.Should.Equal(productName).
                ProductDescriptionAddedToCart.Should.Equal(productDesc).
                Price.Should.Contain(productPrice).
                PaymentInformation.Should.Equal("SauceCard #31337").
                ShippingInformation.Should.Equal("FREE PONY EXPRESS DELIVERY!").
                ItemTotal.Should.Contain(productPrice).
                Tax.Should.Equal(productTax).
                Total.Should.Equal(productTotal).
                CartCheckoutButton.Click();

            Go.To<CompleteCheckoutPage>().
                SubHeaderLabelLabel.Should.Equal("Checkout: Complete!").
                ShoppingCartCounter.Should.Not.BeVisible().
                CompleteHeader.Should.Equal("THANK YOU FOR YOUR ORDER").
                CompleteText.Should.Equal("Your order has been dispatched, and will arrive just as fast as the pony can get there!").
                PonyExpress.IsLoaded.Should.BeTrue();

        }   
    }
}
