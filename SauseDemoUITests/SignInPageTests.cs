﻿using System;
using Atata;
using NUnit.Allure.Core;
using NUnit.Framework;

namespace SauseDemoUITests
{
    [AllureNUnit]
    public class SignInPageTests : UITestFixture
    {
        [Test]
        public void SignIn()
        {
            Login();
        }

        [Test]
        public void SignIn_Validation_RequiredPassword()
        {
            Go.To<SignInPage>().
                UserName.Set(Config.Account.UserName).
                Login();
            Go.To<SignInPage>().Message.Should.Contain("Password is required");
        }

        [Test]
        public void SignIn_Validation_RequiredUsername()
        {
            Go.To<SignInPage>().
                Password.Set(Config.Account.Password).
                Login();
            Go.To<SignInPage>().Message.Should.Contain("Username is required");
        }

        [Test]
        public void SignIn_Validation_InvalidPassword()
        {
            Go.To<SignInPage>().
                UserName.Set(Config.Account.UserName).
                Password.Set(Config.Account.Password + "test").
                Login();
            Go.To<SignInPage>().Message.Should.Contain("Username and password do not match any user in this service");
        }

        [Test]
        public void SignIn_Validation_InvalidUsername()
        {
            Go.To<SignInPage>().
                UserName.Set(Config.Account.UserName + "test").
                Password.Set(Config.Account.Password).
                Login();
            Go.To<SignInPage>().Message.Should.Contain("Username and password do not match any user in this service");
        }


    }
}
