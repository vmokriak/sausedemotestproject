﻿using Atata;
using NUnit.Framework;


namespace SauseDemoUITests
{
    [TestFixture]
    public class UITestFixture
    {
        [SetUp]
        public void SetUp()
        {
            AtataContext.Configure().
                UseChrome().
                    WithArguments("disable-extensions", "start-maximized", "disable-infobars").
                UseBaseUrl(Config.Url).
                UseNUnitTestName().
                AddNUnitTestContextLogging().
                    WithMinLevel(LogLevel.Info).
                    WithoutSectionFinish().
                AddNLogLogging().
                AddScreenshotFileSaving().
                    WithFolderPath(() => $@"Logs\{AtataContext.BuildStart:yyyy-MM-dd HH_mm_ss}\{AtataContext.Current.TestName}").
                LogNUnitError().
                TakeScreenshotOnNUnitError().
                Build();
        }

        [TearDown]
        public void TearDown()
        {
            if (AtataContext.Current != null)
                AtataContext.Current.CleanUp();
        }

        protected ProductsPage Login()
        {
               return Go.To<SignInPage>().
               UserName.Set(Config.Account.UserName).
               Password.Set(Config.Account.Password).
               Login();
        }
    }
}
