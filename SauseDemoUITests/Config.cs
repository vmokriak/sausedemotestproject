﻿using System.Configuration;

namespace SauseDemoUITests
{
    public static class Config
    {
        public static string Url { get; } = ConfigurationManager.AppSettings[nameof(Url)];

        public static class Account
        {
            public static string UserName { get; } = ConfigurationManager.AppSettings[nameof(UserName)];

            public static string Password { get; } = ConfigurationManager.AppSettings[nameof(Password)];
        }
    }
}
