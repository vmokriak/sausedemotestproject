﻿using Atata;

namespace SauseDemoUITests
{
    using _ = SignInPage;

    public class SignInPage : AppPage<_>
    {
        [FindByAttribute("data-test")]
        [Term("username")]
        public TextInput<_> UserName { get; private set; }

        [FindByAttribute("data-test")]
        [Term("password")]
        public PasswordInput<_> Password { get; private set; }

        [FindByClass("login-button")]
        [ControlDefinition("input")]
        public LinkDelegate<ProductsPage, _> Login { get; private set; }

        [FindByXPath("//h3")]
        [TermFindSettings(Match = TermMatch.Contains)]
        public Text<_> Message { get; private set; }

    }
}
