﻿using Atata;

namespace SauseDemoUITests.Components
{
    using _ = OverviewCheckoutPage;

    public class OverviewCheckoutPage : AppPage<_>
    {
        [FindByXPath("//div[@class='summary_info_label'] [contains(text(),'Payment Information')]/following-sibling::div[1]")]
        public Text<_> PaymentInformation { get; private set; }

        [FindByXPath("//div[@class='summary_info_label'] [contains(text(),'Shipping Information')]/following-sibling::div[1]")]
        public Text<_> ShippingInformation { get; private set; }

        [FindByClass("summary_subtotal_label")]
        public Text<_> ItemTotal { get; private set; }

        [FindByClass("summary_tax_label")]
        public Text<_> Tax { get; private set; }

        [FindByClass("summary_total_label")]
        public Text<_> Total { get; private set; }

    }
}
