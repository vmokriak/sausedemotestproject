﻿using Atata;

namespace SauseDemoUITests.Components
{
    using _ = CompleteCheckoutPage;

    public class CompleteCheckoutPage : AppPage<_>
    {
        [FindByClass("complete-header")]
        public Text<_> CompleteHeader { get; private set; }

        [FindByClass("complete-text")]
        public Text<_> CompleteText { get; private set; }

        [FindByClass("checkout_complete_container")]
        [ControlDefinition("img")]
        public Image<_> PonyExpress { get; private set; }
    }
}
