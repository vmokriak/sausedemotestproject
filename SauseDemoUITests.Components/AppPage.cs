﻿using Atata;

namespace SauseDemoUITests
{

    public abstract class AppPage<TOwner> : Page<TOwner>
        where TOwner : AppPage<TOwner>
    {
        [FindByXPath("//span[contains(@class, 'fa-layers-counter')]")]
        public Number<TOwner> ShoppingCartCounter { get; private set; }
        
        [FindByClass("fa-shopping-cart")]
        public Clickable<TOwner> ShoppingCartLink { get; private set; }

        [FindByClass("subheader_label")]
        public Text<TOwner> SubHeaderLabelLabel { get; private set; }

        [FindByClass("inventory_item_name")]
        public Text<TOwner> ProductNameAddedToCart { get; private set; }

        [FindByClass("inventory_item_desc")]
        public Text<TOwner> ProductDescriptionAddedToCart { get; private set; }

        [FindByClass("inventory_item_price")]
        public Text<TOwner> Price { get; private set; }

        [FindByClass("cart_checkout_link")]
        public Clickable<TOwner> CartCheckoutButton { get; private set; }
    }
}
