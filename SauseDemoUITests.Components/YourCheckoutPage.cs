﻿using Atata;

namespace SauseDemoUITests.Components
{
    using _ = YourCheckoutPage;

    public class YourCheckoutPage : AppPage<_>
    {
        [FindByCss("[data-test='firstName']")]
        public TextInput<_> FirstName { get; private set; }

        [FindByCss("[data-test='lastName']")]
        public TextInput<_> LastName { get; private set; }

        [FindByCss("[data-test='postalCode']")]
        public TextInput<_> PostalCode { get; private set; }

    }
}
