﻿using Atata;

namespace SauseDemoUITests.Components
{
    using _ = YourCartPage;

    public class YourCartPage : AppPage<_>
    {
        [FindByClass("cart_checkout_link")]
        public Clickable<_> CheckoutButton { get; private set; }
    }
}
