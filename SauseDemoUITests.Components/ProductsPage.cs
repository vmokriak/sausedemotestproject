﻿using Atata;

namespace SauseDemoUITests
{
    using _ = ProductsPage;

    public class ProductsPage : AppPage<_>
    {
        [FindByClass("product_label")]
        public Text<_> ProductsLabel { get; private set; }

        public ControlList<ProductItem, _> Products { get; private set; }

        [ControlDefinition("div", ContainingClass = "inventory_item")]
        public class ProductItem : Control<_>
        {
 
            [FindByClass("add-to-cart-button")]
            public Clickable<_> AddToCartButton { get; private set; }

            [FindByClass("inventory_item_price")]
            public Text<_> Price { get; private set; }

        }
    }
}
